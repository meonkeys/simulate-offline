# simulate-offline

Simulate offline behavior with a network-disabled Docker container.

1. Build image with `make build`
1. Start container with `make run`
1. In the container, run `python /work/rtest.py`

This should print out something like

    Something went wrong... HTTPConnectionPool(host='adammonsen.com', port=80): Max retries exceeded with url: / (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7fdbf57dfb70>: Failed to establish a new connection: [Errno -3] Temporary failure in name resolution'))

License: AGPL v3

Copyright (C) 2018 Adam Monsen
