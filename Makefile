run:
	docker run --network=none --rm -it -v "$(pwd):/work" --name adam adam /bin/bash

build:
	docker build -t adam .
